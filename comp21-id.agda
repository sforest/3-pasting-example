
-- (2,1)-compatibility between compositions and identities proof
--
-- authors: Simon Forest and Samuel Mimram

open import Relation.Binary.HeterogeneousEquality
open import 3cat
open import ex

comp21-id : is-comp21-id C
comp21-id (C2-xx x) G = refl
comp21-id (C2-xy (C2-αβ-id a)) (C2-xy α₁) = refl
comp21-id (C2-xy (C2-αβ-α x)) (C2-xy (C2-αβ-id .C1-b)) = refl
comp21-id (C2-xy (C2-αβ-α x)) (C2-xy (C2-αβ-β x₁)) = refl
comp21-id (C2-xy (C2-αβ-β x)) (C2-xy (C2-αβ-id .C1-c)) = refl
comp21-id (C2-xy (C2-αβ-αβ x x₁)) (C2-xy (C2-αβ-id .C1-c)) = refl
comp21-id (C2-yz (C2-γδ-id a)) (C2-yz α) = refl
comp21-id (C2-yz (C2-γδ-γ x)) (C2-yz (C2-γδ-id .C1-e)) = refl
comp21-id (C2-yz (C2-γδ-γ x)) (C2-yz (C2-γδ-δ x₁)) = refl
comp21-id (C2-yz (C2-γδ-δ x)) (C2-yz (C2-γδ-id .C1-f)) = refl
comp21-id (C2-yz (C2-γδ-γδ x x₁)) (C2-yz (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-id a) (C2-γδ-id a₁)) (C2-xz (C2-αβ-id .a) (C2-γδ-id .a₁)) = refl
comp21-id (C2-xz (C2-αβ-id a) (C2-γδ-id .C1-d)) (C2-xz (C2-αβ-id .a) (C2-γδ-γ x)) = refl
comp21-id (C2-xz (C2-αβ-id a) (C2-γδ-id .C1-e)) (C2-xz (C2-αβ-id .a) (C2-γδ-δ x)) = refl
comp21-id (C2-xz (C2-αβ-id a) (C2-γδ-id .C1-d)) (C2-xz (C2-αβ-id .a) (C2-γδ-γδ x x₁)) = refl 
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id a₁)) (C2-xz (C2-αβ-α x) (C2-γδ-id .a₁)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-d)) (C2-xz (C2-αβ-α x) (C2-γδ-γ x₁)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-e)) (C2-xz (C2-αβ-α x) (C2-γδ-δ x₁)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-d)) (C2-xz (C2-αβ-α x) (C2-γδ-γδ x₁ x₂)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id a₁)) (C2-xz (C2-αβ-β x) (C2-γδ-id .a₁)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-d)) (C2-xz (C2-αβ-β x) (C2-γδ-γ x₁)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-e)) (C2-xz (C2-αβ-β x) (C2-γδ-δ x₁)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-d)) (C2-xz (C2-αβ-β x) (C2-γδ-γδ x₁ x₂)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id a₁)) (C2-xz (C2-αβ-αβ x x₁) (C2-γδ-id .a₁)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-d)) (C2-xz (C2-αβ-αβ x x₁) (C2-γδ-γ x₂)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-e)) (C2-xz (C2-αβ-αβ x x₁) (C2-γδ-δ x₂)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-d)) (C2-xz (C2-αβ-αβ x x₁) (C2-γδ-γδ x₂ x₃)) = refl
comp21-id (C2-xz (C2-αβ-id a) (C2-γδ-γ x)) (C2-xz (C2-αβ-id .a) (C2-γδ-id .C1-e)) = refl
comp21-id (C2-xz (C2-αβ-id a) (C2-γδ-γ x)) (C2-xz (C2-αβ-id .a) (C2-γδ-δ x₁)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-γ x)) (C2-xz (C2-αβ-α x₁) (C2-γδ-id .C1-e)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-γ x)) (C2-xz (C2-αβ-α x₁) (C2-γδ-δ x₂)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-γ x)) (C2-xz (C2-αβ-β x₁) (C2-γδ-id .C1-e)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-γ x)) (C2-xz (C2-αβ-β x₁) (C2-γδ-δ x₂)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-γ x)) (C2-xz (C2-αβ-αβ x₁ x₂) (C2-γδ-id .C1-e)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-γ x)) (C2-xz (C2-αβ-αβ x₁ x₂) (C2-γδ-δ x₃)) = refl
comp21-id (C2-xz (C2-αβ-id a) (C2-γδ-δ x)) (C2-xz (C2-αβ-id .a) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-δ x)) (C2-xz (C2-αβ-α x₁) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-δ x)) (C2-xz (C2-αβ-β x₁) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-δ x)) (C2-xz (C2-αβ-αβ x₁ x₂) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-id a) (C2-γδ-γδ x x₁)) (C2-xz (C2-αβ-id .a) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-γδ x x₁)) (C2-xz (C2-αβ-α x₂) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-γδ x x₁)) (C2-xz (C2-αβ-β x₂) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-γδ x x₁)) (C2-xz (C2-αβ-αβ x₂ x₃) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-α x) (C2-γδ-id a)) (C2-xz (C2-αβ-id .C1-b) C₁) = refl
comp21-id (C2-xz (C2-αβ-α x) (C2-γδ-id a)) (C2-xz (C2-αβ-β x₁) C₁) = refl
comp21-id (C2-xz (C2-αβ-α x) (C2-γδ-γ x₁)) (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-e)) = refl
comp21-id (C2-xz (C2-αβ-α x) (C2-γδ-γ x₁)) (C2-xz (C2-αβ-id .C1-b) (C2-γδ-δ x₂)) = refl
comp21-id (C2-xz (C2-αβ-α x) (C2-γδ-γ x₁)) (C2-xz (C2-αβ-β x₂) (C2-γδ-id .C1-e)) = refl
comp21-id (C2-xz (C2-αβ-α x) (C2-γδ-γ x₁)) (C2-xz (C2-αβ-β x₂) (C2-γδ-δ x₃)) = refl
comp21-id (C2-xz (C2-αβ-α x) (C2-γδ-δ x₁)) (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-α x) (C2-γδ-δ x₁)) (C2-xz (C2-αβ-β x₂) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-α x) (C2-γδ-γδ x₁ x₂)) (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-α x) (C2-γδ-γδ x₁ x₂)) (C2-xz (C2-αβ-β x₃) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-β x) (C2-γδ-id a)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .a)) = refl
comp21-id (C2-xz (C2-αβ-β x) (C2-γδ-id .C1-d)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-γ x₁)) = refl
comp21-id (C2-xz (C2-αβ-β x) (C2-γδ-id .C1-e)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-δ x₁)) = refl
comp21-id (C2-xz (C2-αβ-β x) (C2-γδ-id .C1-d)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-γδ x₁ x₂)) = refl
comp21-id (C2-xz (C2-αβ-β x) (C2-γδ-γ x₁)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-e)) = refl
comp21-id (C2-xz (C2-αβ-β x) (C2-γδ-γ x₁)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-δ x₂)) = refl
comp21-id (C2-xz (C2-αβ-β x) (C2-γδ-δ x₁)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-β x) (C2-γδ-γδ x₁ x₂)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-αβ x x₁) (C2-γδ-id a)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .a)) = refl
comp21-id (C2-xz (C2-αβ-αβ x x₁) (C2-γδ-id .C1-d)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-γ x₂)) = refl
comp21-id (C2-xz (C2-αβ-αβ x x₁) (C2-γδ-id .C1-e)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-δ x₂)) = refl
comp21-id (C2-xz (C2-αβ-αβ x x₁) (C2-γδ-id .C1-d)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-γδ x₂ x₃)) = refl
comp21-id (C2-xz (C2-αβ-αβ x x₁) (C2-γδ-γ x₂)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-e)) = refl
comp21-id (C2-xz (C2-αβ-αβ x x₁) (C2-γδ-γ x₂)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-δ x₃)) = refl
comp21-id (C2-xz (C2-αβ-αβ x x₁) (C2-γδ-δ x₂)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f)) = refl
comp21-id (C2-xz (C2-αβ-αβ x x₁) (C2-γδ-γδ x₂ x₃)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f)) = refl
