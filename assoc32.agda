
-- (3,2)-associativity proof
--
-- authors: Simon Forest and Samuel Mimram

open import Relation.Binary.HeterogeneousEquality
open import 3cat
open import ex

assoc32 : is-assoc32 C
assoc32 (C3-id α) γ η = refl
assoc32 C3-Ψ (C3-id .(C2-xz (C2-αβ-α C2-α') (C2-γδ-δ C2-δ'))) η = refl
assoc32 (C3-Ψ-β β) (C3-id .(C2-xz (C2-αβ-αβ C2-α' β) (C2-γδ-δ C2-δ'))) η = refl
assoc32 (C3-Ψ-γ C₁) (C3-id .(C2-xz (C2-αβ-α C2-α') (C2-γδ-γδ C₁ C2-δ'))) η = refl
assoc32 (C3-Ψ-βγ β C₁) (C3-id .(C2-xz (C2-αβ-αβ C2-α' β) (C2-γδ-γδ C₁ C2-δ'))) η = refl
assoc32 (C3-Ψ-βγ .C2-β .C2-γ) (C3-Φ-αδ .C2-α' .C2-δ') (C3-id .(C2-xz (C2-αβ-αβ C2-α' C2-β') (C2-γδ-γδ C2-γ' C2-δ'))) = refl
assoc32 C3-Φ (C3-id .(C2-xz (C2-αβ-β C2-β') (C2-γδ-γ C2-γ'))) η = refl
assoc32 (C3-Φ-α α) (C3-id .(C2-xz (C2-αβ-αβ α C2-β') (C2-γδ-γ C2-γ'))) η = refl
assoc32 (C3-Φ-δ δ) (C3-id .(C2-xz (C2-αβ-β C2-β') (C2-γδ-γδ C2-γ' δ))) η = refl
assoc32 (C3-Φ-αδ α δ) (C3-id .(C2-xz (C2-αβ-αβ α C2-β') (C2-γδ-γδ C2-γ' δ))) η = refl
assoc32 (C3-Φ-αδ .C2-α .C2-δ) (C3-Ψ-βγ .C2-β' .C2-γ') (C3-id .(C2-xz (C2-αβ-αβ C2-α' C2-β') (C2-γδ-γδ C2-γ' C2-δ'))) = refl
assoc32 C3-ΨΦ (C3-id .(C2-xz (C2-αβ-αβ C2-α' C2-β') (C2-γδ-γδ C2-γ' C2-δ'))) η = refl
assoc32 C3-ΦΨ (C3-id .(C2-xz (C2-αβ-αβ C2-α' C2-β') (C2-γδ-γδ C2-γ' C2-δ'))) η = refl
