
-- (2,0)-associativity proof
--
-- authors: Simon Forest and Samuel Mimram

open import Relation.Binary.HeterogeneousEquality
open import 3cat
open import ex

assoc20 : is-assoc20 C
assoc20 (C2-xx x) G H = refl
assoc20 (C2-xy α) (C2-xx .C0-y) H = refl
assoc20 (C2-xy (C2-αβ-id a)) (C2-yz (C2-γδ-id a₁)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-id a)) (C2-yz (C2-γδ-γ γ)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-id a)) (C2-yz (C2-γδ-δ δ)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-id a)) (C2-yz (C2-γδ-γδ γ δ)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-α α)) (C2-yz (C2-γδ-id a)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-α α)) (C2-yz (C2-γδ-γ γ)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-α α)) (C2-yz (C2-γδ-δ δ)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-α α)) (C2-yz (C2-γδ-γδ γ δ)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-β β)) (C2-yz (C2-γδ-id a)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-β β)) (C2-yz (C2-γδ-γ γ)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-β β)) (C2-yz (C2-γδ-δ δ)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-β β)) (C2-yz (C2-γδ-γδ γ δ)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-αβ α β)) (C2-yz (C2-γδ-id a)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-αβ α β)) (C2-yz (C2-γδ-γ γ)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-αβ α β)) (C2-yz (C2-γδ-δ δ)) (C2-xx .C0-z) = refl
assoc20 (C2-xy (C2-αβ-αβ α β)) (C2-yz (C2-γδ-γδ γ d)) (C2-xx .C0-z) = refl
assoc20 (C2-yz (C2-γδ-id a)) (C2-xx .C0-z) H = refl
assoc20 (C2-yz (C2-γδ-γ γ)) (C2-xx .C0-z) H = refl
assoc20 (C2-yz (C2-γδ-δ δ)) (C2-xx .C0-z) H = refl
assoc20 (C2-yz (C2-γδ-γδ γ δ)) (C2-xx .C0-z) H = refl
assoc20 (C2-xz α γ) (C2-xx .C0-z) H = refl
