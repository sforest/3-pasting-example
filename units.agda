
-- proofs about units
--
-- authors: Simon Forest and Samuel Mimram

open import Relation.Binary.HeterogeneousEquality
open import 3cat
open import ex

unit10-l : is-unit10-l C
unit10-l = refl

unit20-l : is-unit20-l C
unit20-l = refl

unit21-l : is-unit21-l C
unit21-l {F = C2-xx x} = refl
unit21-l {F = C2-xy α} = refl
unit21-l {F = C2-yz α} = refl
unit21-l {F = C2-xz α C₁} = refl

unit30-l : is-unit30-l C
unit30-l {φ = C3-id α} = refl
unit30-l {φ = C3-Ψ} = refl
unit30-l {φ = C3-Ψ-β β} = refl
unit30-l {φ = C3-Ψ-γ C₁} = refl
unit30-l {φ = C3-Ψ-βγ β C₁} = refl
unit30-l {φ = C3-Φ} = refl
unit30-l {φ = C3-Φ-α α} = refl
unit30-l {φ = C3-Φ-δ δ} = refl
unit30-l {φ = C3-Φ-αδ α δ} = refl
unit30-l {φ = C3-ΨΦ} = refl
unit30-l {φ = C3-ΦΨ} = refl

unit31-l : is-unit31-l C
unit31-l {φ = C3-id (C2-xx x)} = refl
unit31-l {φ = C3-id (C2-xy α)} = refl
unit31-l {φ = C3-id (C2-yz α)} = refl
unit31-l {φ = C3-id (C2-xz α (C2-γδ-id a))} = refl
unit31-l {φ = C3-id (C2-xz α (C2-γδ-γ x))} = refl
unit31-l {φ = C3-id (C2-xz α (C2-γδ-δ x))} = refl
unit31-l {φ = C3-id (C2-xz α (C2-γδ-γδ x x₁))} = refl
unit31-l {φ = C3-Ψ} = refl
unit31-l {φ = C3-Ψ-β β} = refl
unit31-l {φ = C3-Ψ-γ C₁} = refl
unit31-l {φ = C3-Ψ-βγ β C₁} = refl
unit31-l {φ = C3-Φ} = refl
unit31-l {φ = C3-Φ-α α} = refl
unit31-l {φ = C3-Φ-δ δ} = refl
unit31-l {φ = C3-Φ-αδ α δ} = refl
unit31-l {φ = C3-ΨΦ} = refl
unit31-l {φ = C3-ΦΨ} = refl

unit32-l : is-unit32-l C
unit32-l {φ = C3-id α} = refl
unit32-l {φ = C3-Ψ} = refl
unit32-l {φ = C3-Ψ-β β} = refl
unit32-l {φ = C3-Ψ-γ C₁} = refl
unit32-l {φ = C3-Ψ-βγ β C₁} = refl
unit32-l {φ = C3-Φ} = refl
unit32-l {φ = C3-Φ-α α} = refl
unit32-l {φ = C3-Φ-δ δ} = refl
unit32-l {φ = C3-Φ-αδ α δ} = refl
unit32-l {φ = C3-ΨΦ} = refl
unit32-l {φ = C3-ΦΨ} = refl

unit10-r : is-unit10-r C
unit10-r (C1-id x) = refl
unit10-r (C1-xy x) = refl
unit10-r (C1-yz x) = refl
unit10-r (C1-xz x x₁) = refl

unit20-r : is-unit20-r C
unit20-r {F = C2-xx x} = refl
unit20-r {F = C2-xy α} = refl
unit20-r {F = C2-yz α} = refl
unit20-r {F = C2-xz α γ} = refl

unit21-r : is-unit21-r C
unit21-r {F = C2-xx x} = refl
unit21-r {F = C2-xy (C2-αβ-id a)} = refl
unit21-r {F = C2-xy (C2-αβ-α α)} = refl
unit21-r {F = C2-xy (C2-αβ-β β)} = refl
unit21-r {F = C2-xy (C2-αβ-αβ α β)} = refl
unit21-r {F = C2-yz (C2-γδ-id a)} = refl
unit21-r {F = C2-yz (C2-γδ-γ γ)} = refl
unit21-r {F = C2-yz (C2-γδ-δ δ)} = refl
unit21-r {F = C2-yz (C2-γδ-γδ γ δ)} = refl
unit21-r {F = C2-xz (C2-αβ-id a) (C2-γδ-id a₁)} = refl
unit21-r {F = C2-xz (C2-αβ-id a) (C2-γδ-γ x)} = refl
unit21-r {F = C2-xz (C2-αβ-id a) (C2-γδ-δ x)} = refl
unit21-r {F = C2-xz (C2-αβ-id a) (C2-γδ-γδ x x₁)} = refl
unit21-r {F = C2-xz (C2-αβ-α x) (C2-γδ-id a)} = refl
unit21-r {F = C2-xz (C2-αβ-α x) (C2-γδ-γ x₁)} = refl
unit21-r {F = C2-xz (C2-αβ-α x) (C2-γδ-δ x₁)} = refl
unit21-r {F = C2-xz (C2-αβ-α x) (C2-γδ-γδ x₁ x₂)} = refl
unit21-r {F = C2-xz (C2-αβ-β x) (C2-γδ-id a)} = refl
unit21-r {F = C2-xz (C2-αβ-β x) (C2-γδ-γ x₁)} = refl
unit21-r {F = C2-xz (C2-αβ-β x) (C2-γδ-δ x₁)} = refl
unit21-r {F = C2-xz (C2-αβ-β x) (C2-γδ-γδ x₁ x₂)} = refl
unit21-r {F = C2-xz (C2-αβ-αβ x x₁) (C2-γδ-id a)} = refl
unit21-r {F = C2-xz (C2-αβ-αβ x x₁) (C2-γδ-γ x₂)} = refl
unit21-r {F = C2-xz (C2-αβ-αβ x x₁) (C2-γδ-δ x₂)} = refl
unit21-r {F = C2-xz (C2-αβ-αβ x x₁) (C2-γδ-γδ x₂ x₃)} = refl

unit30-r : is-unit30-r C
unit30-r {φ = C3-id (C2-xx x)} = refl
unit30-r {φ = C3-id (C2-xy α)} = refl
unit30-r {φ = C3-id (C2-yz α)} = refl
unit30-r {φ = C3-id (C2-xz α C₁)} = refl
unit30-r {φ = C3-Ψ} = refl
unit30-r {φ = C3-Ψ-β β} = refl
unit30-r {φ = C3-Ψ-γ C₁} = refl
unit30-r {φ = C3-Ψ-βγ β C₁} = refl
unit30-r {φ = C3-Φ} = refl
unit30-r {φ = C3-Φ-α α} = refl
unit30-r {φ = C3-Φ-δ δ} = refl
unit30-r {φ = C3-Φ-αδ α δ} = refl
unit30-r {φ = C3-ΨΦ} = refl
unit30-r {φ = C3-ΦΨ} = refl

unit31-r : is-unit31-r C
unit31-r {φ = C3-id (C2-xx x)} = refl
unit31-r {φ = C3-id (C2-xy (C2-αβ-id a))} = refl
unit31-r {φ = C3-id (C2-xy (C2-αβ-α α))} = refl
unit31-r {φ = C3-id (C2-xy (C2-αβ-β β))} = refl
unit31-r {φ = C3-id (C2-xy (C2-αβ-αβ α β))} = refl
unit31-r {φ = C3-id (C2-yz (C2-γδ-id a))} = refl
unit31-r {φ = C3-id (C2-yz (C2-γδ-γ γ))} = refl
unit31-r {φ = C3-id (C2-yz (C2-γδ-δ δ))} = refl
unit31-r {φ = C3-id (C2-yz (C2-γδ-γδ γ δ))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-id a) (C2-γδ-id a₁))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-id a) (C2-γδ-γ γ))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-id a) (C2-γδ-δ δ))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-id a) (C2-γδ-γδ γ δ))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-α α) (C2-γδ-id a))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-α α) (C2-γδ-γ γ))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-α α) (C2-γδ-δ δ))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-α α) (C2-γδ-γδ γ δ))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-β β) (C2-γδ-id a))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-β β) (C2-γδ-γ γ))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-β β) (C2-γδ-δ γ))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-β β) (C2-γδ-γδ γ δ))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-id a))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-γ γ))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-δ γ))} = refl
unit31-r {φ = C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-γδ γ δ))} = refl
unit31-r {φ = C3-Ψ} = refl
unit31-r {φ = C3-Ψ-β β} = refl
unit31-r {φ = C3-Ψ-γ C₁} = refl
unit31-r {φ = C3-Ψ-βγ β C₁} = refl
unit31-r {φ = C3-Φ} = refl
unit31-r {φ = C3-Φ-α α} = refl
unit31-r {φ = C3-Φ-δ δ} = refl
unit31-r {φ = C3-Φ-αδ α δ} = refl
unit31-r {φ = C3-ΨΦ} = refl
unit31-r {φ = C3-ΦΨ} = refl

unit32-r : is-unit32-r C
unit32-r {φ = C3-id α} = refl
unit32-r {φ = C3-Ψ} = refl
unit32-r {φ = C3-Ψ-β β} = refl
unit32-r {φ = C3-Ψ-γ C₁} = refl
unit32-r {φ = C3-Ψ-βγ β C₁} = refl
unit32-r {φ = C3-Φ} = refl
unit32-r {φ = C3-Φ-α α} = refl
unit32-r {φ = C3-Φ-δ δ} = refl
unit32-r {φ = C3-Φ-αδ α δ} = refl
unit32-r {φ = C3-ΨΦ} = refl
unit32-r {φ = C3-ΦΨ} = refl
