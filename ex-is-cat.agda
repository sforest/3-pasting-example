
-- Proof that the example satisfies the axioms of omega-categories
--
-- authors: Simon Forest and Samuel Mimram

open import Relation.Binary.HeterogeneousEquality
open import Relation.Nullary
open import 3cat
open import ex

open import units
open import assoc10
open import assoc20
open import assoc21
open import assoc30
open import assoc31
open import assoc32
open import comp10-id
open import comp20-id
open import comp21-id
open import ich210
open import ich310
open import ich320
open import ich321


-- Proof that the 3PCat C is in fact a 3-category

D : 3Cat C
3Cat.unit10-l D = unit10-l
3Cat.unit10-r D = unit10-r
3Cat.unit20-l D = unit20-l
3Cat.unit20-r D = unit20-r
3Cat.unit21-l D = unit21-l
3Cat.unit21-r D = unit21-r
3Cat.unit30-l D = unit30-l
3Cat.unit30-r D = unit30-r
3Cat.unit31-l D = unit31-l
3Cat.unit31-r D = unit31-r
3Cat.unit32-l D = unit32-l
3Cat.unit32-r D = unit32-r
3Cat.assoc10 D = assoc10
3Cat.assoc20 D = assoc20
3Cat.assoc21 D = assoc21
3Cat.assoc30 D = assoc30
3Cat.assoc31 D = assoc31
3Cat.assoc32 D = assoc32
3Cat.comp10-id D = comp10-id
3Cat.comp20-id D = comp20-id
3Cat.comp21-id D = comp21-id
3Cat.ich210 D = ich210
3Cat.ich310 D = ich310
3Cat.ich320 D = ich320
3Cat.ich321 D = ich321

