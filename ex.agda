

-- Exmple definition
--
-- authors: Simon Forest and Samuel Mimram

open import 3cat

data C0 : Set where
  C0-x : C0
  C0-y : C0
  C0-z : C0

data C1-abc : Set where
  C1-a : C1-abc
  C1-b : C1-abc
  C1-c : C1-abc

data C1-def : Set where
  C1-d : C1-def
  C1-e : C1-def
  C1-f : C1-def

data C1 : C0 → C0 → Set where
  C1-id : (x : C0) → C1 x x
  C1-xy : C1-abc → C1 C0-x C0-y
  C1-yz : C1-def → C1 C0-y C0-z
  C1-xz : C1-abc → C1-def → C1 C0-x C0-z

data C2-αα : Set where
  C2-α : C2-αα
  C2-α' : C2-αα

data C2-ββ : Set where
  C2-β : C2-ββ
  C2-β' : C2-ββ

data C2-γγ : Set where
  C2-γ : C2-γγ
  C2-γ' : C2-γγ

data C2-δδ : Set where
  C2-δ : C2-δδ
  C2-δ' : C2-δδ

-- 2-cells from x to y
data C2-αβ : C1-abc → C1-abc → Set where
  C2-αβ-id : (a : C1-abc) → C2-αβ a a
  C2-αβ-α : C2-αα → C2-αβ C1-a C1-b
  C2-αβ-β : C2-ββ → C2-αβ C1-b C1-c
  C2-αβ-αβ : C2-αα → C2-ββ → C2-αβ C1-a C1-c

-- 2-cells from y to z
data C2-γδ : C1-def → C1-def → Set where
  C2-γδ-id : (a : C1-def) → C2-γδ a a
  C2-γδ-γ : C2-γγ → C2-γδ C1-d C1-e
  C2-γδ-δ : C2-δδ → C2-γδ C1-e C1-f
  C2-γδ-γδ : C2-γγ → C2-δδ → C2-γδ C1-d C1-f

data C2 : {x y : C0} → (a b : C1 x y) → Set where
  C2-xx : (x : C0) → C2 (C1-id x) (C1-id x)
  C2-xy : {a b : C1-abc} (α : C2-αβ a b) → C2 (C1-xy a) (C1-xy b)
  C2-yz : {a b : C1-def} (α : C2-γδ a b) → C2 (C1-yz a) (C1-yz b)
  C2-xz : {a b : C1-abc} {c d : C1-def} (α : C2-αβ a b) (γ : C2-γδ c d) → C2 (C1-xz a c) (C1-xz b d)

data C3 : {x y : C0} {a b : C1 x y} (α β : C2 a b) → Set where
  C3-id : {x y : C0} {a b : C1 x y} (α : C2 a b) → C3 α α
  C3-Ψ : C3 (C2-xz (C2-αβ-α C2-α) (C2-γδ-δ C2-δ)) (C2-xz (C2-αβ-α C2-α') (C2-γδ-δ C2-δ'))
  C3-Ψ-β : (β : C2-ββ) → C3 (C2-xz (C2-αβ-αβ C2-α β) (C2-γδ-δ C2-δ)) (C2-xz (C2-αβ-αβ C2-α' β) (C2-γδ-δ C2-δ'))
  C3-Ψ-γ : (γ : C2-γγ) → C3 (C2-xz (C2-αβ-α C2-α) (C2-γδ-γδ γ C2-δ)) (C2-xz (C2-αβ-α C2-α') (C2-γδ-γδ γ C2-δ'))
  C3-Ψ-βγ : (β : C2-ββ) (γ : C2-γγ) → C3 (C2-xz (C2-αβ-αβ C2-α β) (C2-γδ-γδ γ C2-δ)) (C2-xz (C2-αβ-αβ C2-α' β) (C2-γδ-γδ γ C2-δ'))
  C3-Φ : C3 (C2-xz (C2-αβ-β C2-β) (C2-γδ-γ C2-γ)) (C2-xz (C2-αβ-β C2-β') (C2-γδ-γ C2-γ'))
  C3-Φ-α : (α : C2-αα) → C3 (C2-xz (C2-αβ-αβ α C2-β) (C2-γδ-γ C2-γ)) (C2-xz (C2-αβ-αβ α C2-β') (C2-γδ-γ C2-γ'))
  C3-Φ-δ : (δ : C2-δδ) → C3 (C2-xz (C2-αβ-β C2-β) (C2-γδ-γδ C2-γ δ)) (C2-xz (C2-αβ-β C2-β') (C2-γδ-γδ C2-γ' δ))
  C3-Φ-αδ : (α : C2-αα) (δ : C2-δδ) → C3 (C2-xz (C2-αβ-αβ α C2-β) (C2-γδ-γδ C2-γ δ)) (C2-xz (C2-αβ-αβ α C2-β') (C2-γδ-γδ C2-γ' δ))
  C3-ΨΦ : C3 (C2-xz (C2-αβ-αβ C2-α C2-β) (C2-γδ-γδ C2-γ C2-δ)) (C2-xz (C2-αβ-αβ C2-α' C2-β') (C2-γδ-γδ C2-γ' C2-δ'))
  C3-ΦΨ : C3 (C2-xz (C2-αβ-αβ C2-α C2-β) (C2-γδ-γδ C2-γ C2-δ)) (C2-xz (C2-αβ-αβ C2-α' C2-β') (C2-γδ-γδ C2-γ' C2-δ'))

C : 3PCat C0 C1 C2 C3
3PCat.id0 C x = C1-id x
3PCat.id1 C (C1-id x) = C2-xx x
3PCat.id1 C (C1-xy a) = C2-xy (C2-αβ-id a)
3PCat.id1 C (C1-yz d) = C2-yz (C2-γδ-id d)
3PCat.id1 C (C1-xz a d) = C2-xz (C2-αβ-id a) (C2-γδ-id d)
3PCat.id2 C F = C3-id F
3PCat.comp10 C (C1-id x) g = g
3PCat.comp10 C (C1-xy f) (C1-id .C0-y) = C1-xy f
3PCat.comp10 C (C1-xy f) (C1-yz g) = C1-xz f g
3PCat.comp10 C (C1-yz f) (C1-id .C0-z) = C1-yz f
3PCat.comp10 C (C1-xz f g) (C1-id .C0-z) = C1-xz f g
3PCat.comp20 C (C2-xx x) F = F
3PCat.comp20 C (C2-xy α) (C2-xx .C0-y) = C2-xy α
3PCat.comp20 C (C2-xy α) (C2-yz γ) = C2-xz α γ
3PCat.comp20 C (C2-yz α) (C2-xx .C0-z) = C2-yz α
3PCat.comp20 C (C2-xz α γ) (C2-xx .C0-z) = C2-xz α γ
3PCat.comp21 C (C2-xx x) G = G
3PCat.comp21 C (C2-xy (C2-αβ-id a)) (C2-xy β) = C2-xy β
3PCat.comp21 C (C2-xy (C2-αβ-α α)) (C2-xy (C2-αβ-id .C1-b)) = C2-xy (C2-αβ-α α)
3PCat.comp21 C (C2-xy (C2-αβ-α α)) (C2-xy (C2-αβ-β β)) = C2-xy (C2-αβ-αβ α β)
3PCat.comp21 C (C2-xy (C2-αβ-β β)) (C2-xy (C2-αβ-id .C1-c)) = C2-xy (C2-αβ-β β)
3PCat.comp21 C (C2-xy (C2-αβ-αβ α β)) (C2-xy (C2-αβ-id .C1-c)) = C2-xy (C2-αβ-αβ α β)
3PCat.comp21 C (C2-yz (C2-γδ-id a)) (C2-yz α) = C2-yz α
3PCat.comp21 C (C2-yz (C2-γδ-γ γ)) (C2-yz (C2-γδ-id .C1-e)) = C2-yz (C2-γδ-γ γ)
3PCat.comp21 C (C2-yz (C2-γδ-γ γ)) (C2-yz (C2-γδ-δ δ)) = C2-yz (C2-γδ-γδ γ δ )
3PCat.comp21 C (C2-yz (C2-γδ-δ δ)) (C2-yz (C2-γδ-id .C1-f)) = C2-yz (C2-γδ-δ δ)
3PCat.comp21 C (C2-yz (C2-γδ-γδ γ δ)) (C2-yz (C2-γδ-id .C1-f)) = C2-yz (C2-γδ-γδ γ δ)
3PCat.comp21 C (C2-xz (C2-αβ-id a) (C2-γδ-id d)) (C2-xz β δ) = C2-xz β δ
3PCat.comp21 C (C2-xz (C2-αβ-id a) (C2-γδ-γ γ)) (C2-xz β (C2-γδ-id .C1-e)) = C2-xz β (C2-γδ-γ γ)
3PCat.comp21 C (C2-xz (C2-αβ-id a) (C2-γδ-γ γ)) (C2-xz β (C2-γδ-δ δ)) = C2-xz β (C2-γδ-γδ γ δ)
3PCat.comp21 C (C2-xz (C2-αβ-id a) (C2-γδ-δ δ)) (C2-xz β (C2-γδ-id .C1-f)) = C2-xz β (C2-γδ-δ δ)
3PCat.comp21 C (C2-xz (C2-αβ-id a) (C2-γδ-γδ γ δ)) (C2-xz β (C2-γδ-id .C1-f)) = C2-xz β (C2-γδ-γδ γ δ)
3PCat.comp21 C (C2-xz (C2-αβ-α α) (C2-γδ-id a)) (C2-xz (C2-αβ-id .C1-b) δ) = C2-xz (C2-αβ-α α) δ
3PCat.comp21 C (C2-xz (C2-αβ-α α) (C2-γδ-id a)) (C2-xz (C2-αβ-β β) δ) = C2-xz (C2-αβ-αβ α β) δ
3PCat.comp21 C (C2-xz (C2-αβ-α α) (C2-γδ-γ γ)) (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-e)) = C2-xz (C2-αβ-α α) (C2-γδ-γ γ)
3PCat.comp21 C (C2-xz (C2-αβ-α α) (C2-γδ-γ γ)) (C2-xz (C2-αβ-id .C1-b) (C2-γδ-δ δ)) = C2-xz (C2-αβ-α α) (C2-γδ-γδ γ δ)
3PCat.comp21 C (C2-xz (C2-αβ-α α) (C2-γδ-γ γ)) (C2-xz (C2-αβ-β β) (C2-γδ-id .C1-e)) = C2-xz (C2-αβ-αβ α β) (C2-γδ-γ γ)
3PCat.comp21 C (C2-xz (C2-αβ-α α) (C2-γδ-γ γ)) (C2-xz (C2-αβ-β β) (C2-γδ-δ δ)) = C2-xz (C2-αβ-αβ α β) (C2-γδ-γδ γ δ)
3PCat.comp21 C (C2-xz (C2-αβ-α α) (C2-γδ-δ δ)) (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-f)) = C2-xz (C2-αβ-α α) (C2-γδ-δ δ)
3PCat.comp21 C (C2-xz (C2-αβ-α α) (C2-γδ-δ δ)) (C2-xz (C2-αβ-β β) (C2-γδ-id .C1-f)) = C2-xz (C2-αβ-αβ α β) (C2-γδ-δ δ)
3PCat.comp21 C (C2-xz (C2-αβ-α α) (C2-γδ-γδ γ δ)) (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-f)) = C2-xz (C2-αβ-α α) (C2-γδ-γδ γ δ)
3PCat.comp21 C (C2-xz (C2-αβ-α α) (C2-γδ-γδ γ δ)) (C2-xz (C2-αβ-β β) (C2-γδ-id .C1-f)) = C2-xz (C2-αβ-αβ α β) (C2-γδ-γδ γ δ)
3PCat.comp21 C (C2-xz (C2-αβ-β β) (C2-γδ-id a)) (C2-xz (C2-αβ-id .C1-c) γ) = C2-xz (C2-αβ-β β) γ
3PCat.comp21 C (C2-xz (C2-αβ-β β) (C2-γδ-γ γ)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-e)) = C2-xz (C2-αβ-β β) (C2-γδ-γ γ)
3PCat.comp21 C (C2-xz (C2-αβ-β β) (C2-γδ-γ γ)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-δ δ)) = C2-xz (C2-αβ-β β) (C2-γδ-γδ γ δ)
3PCat.comp21 C (C2-xz (C2-αβ-β β) (C2-γδ-δ δ)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f)) = C2-xz (C2-αβ-β β) (C2-γδ-δ δ)
3PCat.comp21 C (C2-xz (C2-αβ-β β) (C2-γδ-γδ γ δ)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f)) = C2-xz (C2-αβ-β β) (C2-γδ-γδ γ δ)
3PCat.comp21 C (C2-xz (C2-αβ-αβ α β) (C2-γδ-id a)) (C2-xz (C2-αβ-id .C1-c) γ) = C2-xz (C2-αβ-αβ α β) γ
3PCat.comp21 C (C2-xz (C2-αβ-αβ α β) (C2-γδ-γ γ)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-e)) = C2-xz (C2-αβ-αβ α β) (C2-γδ-γ γ)
3PCat.comp21 C (C2-xz (C2-αβ-αβ α β) (C2-γδ-γ γ)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-δ δ)) = C2-xz (C2-αβ-αβ α β) (C2-γδ-γδ γ δ)
3PCat.comp21 C (C2-xz (C2-αβ-αβ α β) (C2-γδ-δ δ)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f)) = C2-xz (C2-αβ-αβ α β) (C2-γδ-δ δ)
3PCat.comp21 C (C2-xz (C2-αβ-αβ α β) (C2-γδ-γδ γ δ)) (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f)) = C2-xz (C2-αβ-αβ α β) (C2-γδ-γδ γ δ)
3PCat.comp30 C (C3-id α) (C3-id β) = C3-id (3PCat.comp20 C α β)
3PCat.comp30 C (C3-id (C2-xx .C0-x)) C3-Ψ = C3-Ψ
3PCat.comp30 C (C3-id (C2-xx .C0-x)) (C3-Ψ-β β) = C3-Ψ-β β
3PCat.comp30 C (C3-id (C2-xx .C0-x)) (C3-Ψ-γ γ) = C3-Ψ-γ γ
3PCat.comp30 C (C3-id (C2-xx .C0-x)) (C3-Ψ-βγ β γ) = C3-Ψ-βγ β γ
3PCat.comp30 C (C3-id (C2-xx .C0-x)) C3-Φ = C3-Φ
3PCat.comp30 C (C3-id (C2-xx .C0-x)) (C3-Φ-α α) = C3-Φ-α α
3PCat.comp30 C (C3-id (C2-xx .C0-x)) (C3-Φ-δ δ) = C3-Φ-δ δ
3PCat.comp30 C (C3-id (C2-xx .C0-x)) (C3-Φ-αδ α δ) = C3-Φ-αδ α δ
3PCat.comp30 C C3-Ψ (C3-id (C2-xx .C0-z)) = C3-Ψ
3PCat.comp30 C (C3-Ψ-β β) (C3-id (C2-xx .C0-z)) = C3-Ψ-β β
3PCat.comp30 C (C3-Ψ-γ γ) (C3-id (C2-xx .C0-z)) = C3-Ψ-γ γ
3PCat.comp30 C (C3-Ψ-βγ β γ) (C3-id (C2-xx .C0-z)) = C3-Ψ-βγ β γ
3PCat.comp30 C C3-Φ (C3-id (C2-xx .C0-z)) = C3-Φ
3PCat.comp30 C (C3-Φ-α α) (C3-id (C2-xx .C0-z)) = C3-Φ-α α
3PCat.comp30 C (C3-Φ-δ δ) (C3-id (C2-xx .C0-z)) = C3-Φ-δ δ
3PCat.comp30 C (C3-Φ-αδ α δ) (C3-id (C2-xx .C0-z)) = C3-Φ-αδ α δ
3PCat.comp30 C C3-ΨΦ (C3-id (C2-xx .C0-z)) = C3-ΨΦ
3PCat.comp30 C (C3-id (C2-xx .C0-x)) C3-ΨΦ = C3-ΨΦ
3PCat.comp30 C (C3-id (C2-xx .C0-x)) C3-ΦΨ = C3-ΦΨ
3PCat.comp30 C C3-ΦΨ (C3-id (C2-xx .C0-z)) = C3-ΦΨ
3PCat.comp31 C (C3-id (C2-xx x)) (C3-id β) = C3-id β
3PCat.comp31 C (C3-id (C2-xy (C2-αβ-id a))) (C3-id (C2-xy α)) = C3-id (C2-xy α)
3PCat.comp31 C (C3-id (C2-xy (C2-αβ-α α))) (C3-id (C2-xy (C2-αβ-id .C1-b))) = C3-id (C2-xy (C2-αβ-α α))
3PCat.comp31 C (C3-id (C2-xy (C2-αβ-α α))) (C3-id (C2-xy (C2-αβ-β β))) = C3-id (C2-xy (C2-αβ-αβ α β))
3PCat.comp31 C (C3-id (C2-xy (C2-αβ-β β))) (C3-id (C2-xy (C2-αβ-id .C1-c))) = C3-id (C2-xy (C2-αβ-β β))
3PCat.comp31 C (C3-id (C2-xy (C2-αβ-αβ α β))) (C3-id (C2-xy (C2-αβ-id .C1-c))) = C3-id (C2-xy (C2-αβ-αβ α β))
3PCat.comp31 C (C3-id (C2-yz (C2-γδ-id a))) (C3-id (C2-yz α)) = C3-id (C2-yz α)
3PCat.comp31 C (C3-id (C2-yz (C2-γδ-γ γ))) (C3-id (C2-yz (C2-γδ-id .C1-e))) = C3-id (C2-yz (C2-γδ-γ γ))
3PCat.comp31 C (C3-id (C2-yz (C2-γδ-γ γ))) (C3-id (C2-yz (C2-γδ-δ δ))) = C3-id (C2-yz (C2-γδ-γδ γ δ))
3PCat.comp31 C (C3-id (C2-yz (C2-γδ-δ δ))) (C3-id (C2-yz (C2-γδ-id .C1-f))) = C3-id (C2-yz (C2-γδ-δ δ))
3PCat.comp31 C (C3-id (C2-yz (C2-γδ-γδ γ δ))) (C3-id (C2-yz (C2-γδ-id .C1-f))) = C3-id (C2-yz (C2-γδ-γδ γ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id a) (C2-γδ-id .d))) (C3-id (C2-xz β (C2-γδ-id d))) = C3-id (C2-xz β (C2-γδ-id d))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id a) (C2-γδ-γ γ))) (C3-id (C2-xz β (C2-γδ-id .C1-e))) = C3-id (C2-xz β (C2-γδ-γ γ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id a) (C2-γδ-δ δ))) (C3-id (C2-xz β (C2-γδ-id .C1-f))) = C3-id (C2-xz β (C2-γδ-δ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id a) (C2-γδ-γδ γ δ))) (C3-id (C2-xz β (C2-γδ-id .C1-f))) = C3-id (C2-xz β (C2-γδ-γδ γ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id a) (C2-γδ-id .C1-d))) (C3-id (C2-xz β (C2-γδ-γ γ))) = C3-id (C2-xz β (C2-γδ-γ γ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id a) (C2-γδ-id .C1-e))) (C3-id (C2-xz β (C2-γδ-δ δ))) = C3-id (C2-xz β (C2-γδ-δ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id a) (C2-γδ-γ γ))) (C3-id (C2-xz β (C2-γδ-δ δ))) = C3-id (C2-xz β (C2-γδ-γδ γ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id a) (C2-γδ-id .C1-d))) (C3-id (C2-xz β (C2-γδ-γδ γ δ))) = C3-id (C2-xz β (C2-γδ-γδ γ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-α α) (C2-γδ-id a))) (C3-id (C2-xz (C2-αβ-id .C1-b) δ)) = C3-id (C2-xz (C2-αβ-α α) δ)
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-α α) (C2-γδ-γ γ))) (C3-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-e))) = C3-id (C2-xz (C2-αβ-α α) (C2-γδ-γ γ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-α α) (C2-γδ-γ γ))) (C3-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-δ δ))) = C3-id (C2-xz (C2-αβ-α α) (C2-γδ-γδ γ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-α α) (C2-γδ-δ δ))) (C3-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-f))) = C3-id (C2-xz (C2-αβ-α α) (C2-γδ-δ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-α α) (C2-γδ-γδ γ δ))) (C3-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-f))) = C3-id (C2-xz (C2-αβ-α α) (C2-γδ-γδ γ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-α α) (C2-γδ-id a))) (C3-id (C2-xz (C2-αβ-β β) δ)) = C3-id (C2-xz (C2-αβ-αβ α β) δ)
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-α α) (C2-γδ-γ γ))) (C3-id (C2-xz (C2-αβ-β β) (C2-γδ-id .C1-e))) = C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-γ γ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-α α) (C2-γδ-γ γ))) (C3-id (C2-xz (C2-αβ-β β) (C2-γδ-δ δ))) = C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-γδ γ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-α α) (C2-γδ-δ δ))) (C3-id (C2-xz (C2-αβ-β β) (C2-γδ-id .C1-f))) = C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-δ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-α α) (C2-γδ-γδ γ δ))) (C3-id (C2-xz (C2-αβ-β β) (C2-γδ-id .C1-f))) = C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-γδ γ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-β β) (C2-γδ-id a))) (C3-id (C2-xz (C2-αβ-id .C1-c) δ)) = C3-id (C2-xz (C2-αβ-β β) δ)
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-β β) (C2-γδ-γ γ))) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-e))) = C3-id (C2-xz (C2-αβ-β β) (C2-γδ-γ γ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-β β) (C2-γδ-γ γ))) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-δ δ))) = C3-id (C2-xz (C2-αβ-β β) (C2-γδ-γδ γ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-β β) (C2-γδ-δ δ))) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f))) = C3-id (C2-xz (C2-αβ-β β) (C2-γδ-δ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-β β) (C2-γδ-γδ γ δ))) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f))) = C3-id (C2-xz (C2-αβ-β β) (C2-γδ-γδ γ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-id a))) (C3-id (C2-xz (C2-αβ-id .C1-c) δ)) = C3-id (C2-xz (C2-αβ-αβ α β) δ)
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-γ γ))) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-e))) = C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-γ γ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-γ γ))) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-δ δ))) = C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-γδ γ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-δ δ))) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f))) = C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-δ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-γδ γ δ))) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f))) = C3-id (C2-xz (C2-αβ-αβ α β) (C2-γδ-γδ γ δ))
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-e))) C3-Ψ = C3-Ψ
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-γ γ))) C3-Ψ = C3-Ψ-γ γ
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-e))) (C3-Ψ-β β) = C3-Ψ-β β
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-γ γ))) (C3-Ψ-β β) = C3-Ψ-βγ β γ
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-d))) (C3-Ψ-γ γ) = C3-Ψ-γ γ
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-d))) (C3-Ψ-βγ β γ) = C3-Ψ-βγ β γ
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-d))) C3-Φ = C3-Φ
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-α α) (C2-γδ-id .C1-d))) C3-Φ = C3-Φ-α α
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-d))) (C3-Φ-α α) = C3-Φ-α α
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-d))) (C3-Φ-δ δ) = C3-Φ-δ δ
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-α α) (C2-γδ-id .C1-d))) (C3-Φ-δ δ) = C3-Φ-αδ α δ
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-d))) (C3-Φ-αδ α δ) = C3-Φ-αδ α δ
3PCat.comp31 C C3-Ψ (C3-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-f))) = C3-Ψ
3PCat.comp31 C C3-Ψ (C3-id (C2-xz (C2-αβ-β β) (C2-γδ-id .C1-f))) = C3-Ψ-β β
3PCat.comp31 C (C3-Ψ-β β) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f))) = C3-Ψ-β β
3PCat.comp31 C (C3-Ψ-γ γ) (C3-id (C2-xz (C2-αβ-id .C1-b) (C2-γδ-id .C1-f))) = C3-Ψ-γ γ
3PCat.comp31 C (C3-Ψ-γ γ) (C3-id (C2-xz (C2-αβ-β β) (C2-γδ-id .C1-f))) = C3-Ψ-βγ β γ
3PCat.comp31 C (C3-Ψ-βγ β γ) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f))) = C3-Ψ-βγ β γ
3PCat.comp31 C C3-Φ (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-e))) = C3-Φ
3PCat.comp31 C C3-Φ (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-δ δ))) = C3-Φ-δ δ
3PCat.comp31 C (C3-Φ-α α) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-e))) = C3-Φ-α α
3PCat.comp31 C (C3-Φ-α α) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-δ δ))) = C3-Φ-αδ α δ
3PCat.comp31 C (C3-Φ-δ δ) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f))) = C3-Φ-δ δ
3PCat.comp31 C (C3-Φ-αδ α δ) (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f))) = C3-Φ-αδ α δ
3PCat.comp31 C C3-ΨΦ (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f))) = C3-ΨΦ
3PCat.comp31 C C3-ΦΨ (C3-id (C2-xz (C2-αβ-id .C1-c) (C2-γδ-id .C1-f))) = C3-ΦΨ
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-d))) C3-ΨΦ = C3-ΨΦ
3PCat.comp31 C (C3-id (C2-xz (C2-αβ-id .C1-a) (C2-γδ-id .C1-d))) C3-ΦΨ = C3-ΦΨ
3PCat.comp32 C (C3-id α) ψ = ψ
3PCat.comp32 C C3-Ψ (C3-id .(C2-xz (C2-αβ-α C2-α') (C2-γδ-δ C2-δ'))) = C3-Ψ
3PCat.comp32 C (C3-Ψ-β β) (C3-id .(C2-xz (C2-αβ-αβ C2-α' β) (C2-γδ-δ C2-δ'))) = C3-Ψ-β β
3PCat.comp32 C (C3-Ψ-γ γ) (C3-id .(C2-xz (C2-αβ-α C2-α') (C2-γδ-γδ γ C2-δ'))) = C3-Ψ-γ γ
3PCat.comp32 C (C3-Ψ-βγ β γ) (C3-id .(C2-xz (C2-αβ-αβ C2-α' β) (C2-γδ-γδ γ C2-δ'))) = C3-Ψ-βγ β γ
3PCat.comp32 C (C3-Ψ-βγ β γ) (C3-Φ-αδ .C2-α' .C2-δ') = C3-ΨΦ
3PCat.comp32 C C3-Φ (C3-id .(C2-xz (C2-αβ-β C2-β') (C2-γδ-γ C2-γ'))) = C3-Φ
3PCat.comp32 C (C3-Φ-α α) (C3-id .(C2-xz (C2-αβ-αβ α C2-β') (C2-γδ-γ C2-γ'))) = C3-Φ-α α
3PCat.comp32 C (C3-Φ-δ δ) (C3-id .(C2-xz (C2-αβ-β C2-β') (C2-γδ-γδ C2-γ' δ))) = C3-Φ-δ δ
3PCat.comp32 C (C3-Φ-αδ α δ) (C3-id .(C2-xz (C2-αβ-αβ α C2-β') (C2-γδ-γδ C2-γ' δ))) = C3-Φ-αδ α δ
3PCat.comp32 C (C3-Φ-αδ .C2-α .C2-δ) (C3-Ψ-βγ .C2-β' .C2-γ') = C3-ΦΨ
3PCat.comp32 C C3-ΨΦ (C3-id .(C2-xz (C2-αβ-αβ C2-α' C2-β') (C2-γδ-γδ C2-γ' C2-δ'))) = C3-ΨΦ
3PCat.comp32 C C3-ΦΨ (C3-id .(C2-xz (C2-αβ-αβ C2-α' C2-β') (C2-γδ-γδ C2-γ' C2-δ'))) = C3-ΦΨ
