
-- Proof that the example has two different 3-cells
--
-- authors: Simon Forest and Samuel Mimram

open import Relation.Binary.HeterogeneousEquality
open import Relation.Nullary
open import 3cat
open import ex

cella = (C1-xy C1-a)
cellb = (C1-xy C1-b)
cellc = (C1-xy C1-c)
celld = (C1-yz C1-d)
celle = (C1-yz C1-e)
cellf = (C1-yz C1-f)

cellα = C2-xy (C2-αβ-α C2-α)
cellα' = C2-xy (C2-αβ-α C2-α')
cellβ = C2-xy (C2-αβ-β C2-β)
cellβ' = C2-xy (C2-αβ-β C2-β')
cellγ = C2-yz (C2-γδ-γ C2-γ)
cellγ' = C2-yz (C2-γδ-γ C2-γ')
cellδ = C2-yz (C2-γδ-δ C2-δ)
cellδ' = C2-yz (C2-γδ-δ C2-δ')

cellΨ = C3-Ψ
cellΦ = C3-Φ

s : C2 (3PCat.comp10 C cella celld) (3PCat.comp10 C cellc cellf)
s = 3PCat.comp20 C (3PCat.comp21 C cellα cellβ) (3PCat.comp21 C cellγ cellδ)

t : C2 (3PCat.comp10 C cella celld) (3PCat.comp10 C cellc cellf)
t = 3PCat.comp20 C (3PCat.comp21 C cellα' cellβ') (3PCat.comp21 C cellγ' cellδ')

-- Proof that the two compositions "Φ then Ψ" "Φ then Ψ" are different

cellΓ : C3 s t
cellΓ =
  let c1 = 3PCat.comp30 C (3PCat.id2 C cellα) (3PCat.id2 C (3PCat.id1 C celld)) in
  let c2 = 3PCat.comp30 C (3PCat.id2 C (3PCat.id1 C cellc)) (3PCat.id2 C cellδ) in
  let first = 3PCat.comp31 C (3PCat.comp31 C c1 cellΦ) c2 in
  let c3 = 3PCat.comp30 C (3PCat.id2 C (3PCat.id1 C cella)) (3PCat.id2 C cellγ') in
  let c4 = 3PCat.comp30 C (3PCat.id2 C cellβ') (3PCat.id2 C (3PCat.id1 C cellf)) in
  let second = 3PCat.comp31 C (3PCat.comp31 C c3 cellΨ) c4 in
  3PCat.comp32 C first second

cellΔ : C3 s t
cellΔ =
  let c1 = 3PCat.comp30 C (3PCat.id2 C (3PCat.id1 C cella)) (3PCat.id2 C cellγ) in
  let c2 = 3PCat.comp30 C (3PCat.id2 C cellβ) (3PCat.id2 C (3PCat.id1 C cellf)) in
  let first = 3PCat.comp31 C (3PCat.comp31 C c1 cellΨ) c2 in
  let c3 = 3PCat.comp30 C (3PCat.id2 C cellα') (3PCat.id2 C (3PCat.id1 C celld)) in
  let c4 = 3PCat.comp30 C (3PCat.id2 C (3PCat.id1 C cellc)) (3PCat.id2 C cellδ') in
  let second = 3PCat.comp31 C (3PCat.comp31 C c3 cellΦ) c4 in
  3PCat.comp32 C first second

main-lemma : ¬ cellΓ ≅ cellΔ
main-lemma ()
